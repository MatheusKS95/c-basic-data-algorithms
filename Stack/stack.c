/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

void stack_init(struct Stack *stck) //initializes the stack
{
    stck->first = NULL;				//set the first ListItem as NULL because there's nothing yet
    stck->size = 0;					//there's nothing yet == zero
}

int stack_add(struct Stack *stck, int data)
{
    struct StackItem *aux;                      //obviously creating a pointer to StackItem
    aux = malloc(sizeof(struct StackItem));     //allocating some memory for a new StackItem

    if(aux != NULL)
    {
        aux->value = data;                      //add the data to value of aux
        aux->next = stck->first;
        stck->first = aux;                      //set aux as the first item on stack
        stck->size++;                           //if you add a new item you need to update the size value
        return 0;
    }

    return 1;
}

int stack_remove(struct Stack *stck, int *val) 	//get first value and remove it from top (loyal to stack "philosophy")
{
    struct StackItem *aux = stck->first; 		//get the address from first value from the top of the stack
    if(aux == NULL) return 1; 					//if the first is null, returns false

    *val = aux -> value; 						//get the first value
    stck->first = stck->first->next; 			//set the stack's first item the item after the one that we are working with
    stck->size--; 								//there's one less

    free(aux); 									//finally, removes allocated space

    return 0; 									//returns true
}

int stack_checkfirst(struct Stack stck, int *val) 	//only check the first value without removing it from stack (not loyal to stack "philosophy")
{
    if(stck.first == NULL) return 1; 				//if there's nothing, returns 0 (false)
    *val = stck.first -> value; 					//if not, set val as the first item value without removing the item from stack ...
    return 0; 										//... and then returns 1 (true)
}

int stack_size(struct Stack *stck) 	//auto-explicative
{
    return stck->size;
}

void stack_kill(struct Stack *stck) 			//destroy the entire stack without returning any value -- SEGFAULT
{
    while(stck->size != 0)						//while there's values inside the stack...
    {
        struct StackItem *aux = stck->first;	//... you set an auxiliary pointer as the top ...
        if(aux == NULL) break;					//... then you check if it exists ...
        stck->first = stck->first->next;		//... to set the next item as the top
        stck->size--;							//... and decreases the size

        free(aux);								//... then you need to remove the former top
    }											//... and proceeds to remove the next

    if(stck->size == 0)							//if the stack is empty...
    {
        free(stck);								//... remove the stack
    }

    return;
}
