/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main()
{
    struct Stack stacky;
    char opc = 't';

    stack_init(&stacky);

    do
    {
        printf("Please select one option:\n");
        printf("(a) Add to top\n");
        printf("(r) Remove top\n");
        printf("(c) Check top value without removing\n");
        printf("(s) Check stack size\n");
        printf("(x) Exit (warning - exiting with existing stack may cause memory leak)\n");
        scanf(" %c", &opc);

        if(opc == 'a')
        {
            int val2stck;
            printf("Please type an integer value: ");
            scanf("%d", &val2stck);
            printf("\n");
            int stats = stack_add(&stacky, val2stck);
            if(stats != 1)
            {
                printf("\nSuccess!\n");
            }
            else
            {
                printf("\nFail...\n");
            }
        }
        if(opc == 'r')
        {
            int valfromstck;
            int stats = stack_remove(&stacky, &valfromstck);
            if(stats != 1)
            {
                printf("\nSuccess! Value removed: %d\n", valfromstck);
            }
            else
            {
                printf("\nFail...\n");
            }
        }
        if(opc == 'c')
        {
            int valfromstck;
            int stats = stack_checkfirst(stacky, &valfromstck);
            if(stats != 1)
            {
                printf("\nSuccess! Value on top: %d\n", valfromstck);
            }
            else
            {
                printf("\nFail...\n");
            }
        }
        if(opc == 's')
        {
            int sizestck = stack_size(&stacky);
            printf("\nStack size: %d\n", sizestck);
        }
    }while(opc != 'x');

    //stack_kill(&stacky);

    printf("\n\nDemo created by Matheus K. Schaefer (2020)\n");
    return 0;
}
