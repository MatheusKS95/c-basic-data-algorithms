# Basic data algorithms in C #

* This repo have some basic data algorithms written in C (Stack, Queue and Doubly-linked List for now). It was created intended to be easy to read and easy to understand.
* If you are a Computer Science student, this can help you.

### How do I get set up? ###

* Stack, Queue and List uses CMake as build method. If you are on Linux you can run on CLI ``` mkdir build && cd build && cmake .. && make ``` or if you wish or you are on Windows, you can use CMake GUI.
* All projects can be opened on any IDE that supports CMake (such as Qt Creator).
* You can run using GCC if you wish.

### Contribution guidelines ###

* If you want to contribute, feel free. But use a separate branch in order to keep things organized until all changes are accepted.
* Keep the code simple and comment every (or most) line if possible.

### Repo "manager" ###

* Matheus Klein Schaefer
