/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

int main()
{
    printf("QUEUE");

    struct Queue fifo;
    char opc = 'k';

    queue_init(&fifo);

    do{
        int vals;
        int ret;
        printf("\na - Add\nr - Remove\nc - Check first\ns - Queue size\nx - Close (please check if queue is empty before exiting)\n");
        scanf(" %c", &opc);
        switch (opc) {
        case 'a':
            printf("\nPlease add an value: ");
            scanf("%d", &vals);
            ret = queue_add(&fifo, vals);
            if(ret == 0)
            {
                printf("Value %d sucessfully added to queue!\n", vals);
            }
            else
            {
                printf("Error. Value couldn't be added...\n");
            }
            break;
        case 'r':
            ret = queue_remove(&fifo, &vals);
            if(ret == 0)
            {
                printf("Value %d removed from queue!\n", vals);
            }
            else
            {
                printf("Error. There are any values on the queue?\n");
            }
            break;
        case 'c':
            ret = queue_checkfirst(fifo, &vals);
            if(ret == 0)
            {
                printf("First: %d\n", vals);
            }
            else
            {
                printf("There are no values in the queue.\n");
            }
            break;
        case 's':
            printf("Size: %d\n", queue_size(fifo));
            break;
        default:
            break;
        }
    }while(opc != 'x');

    return 0;
}
