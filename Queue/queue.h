/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef QUEUE_C
#define QUEUE_C

struct QueueItem
{
    int value;					//if you need another type, you can change
    struct QueueItem *next;		//a pointer to the next one
};

struct Queue
{
    int size;					//store the size of the queue
    struct QueueItem *first;	//a pointer to the first element of the queue
    struct QueueItem *last;		//a pointer to the last one
};

void queue_init(struct Queue *queue);
int queue_add(struct Queue *queue, int data);
int queue_checkfirst(struct Queue queue, int *data);
int queue_remove(struct Queue *queue, int *data);
int queue_size(struct Queue queue);

#endif
