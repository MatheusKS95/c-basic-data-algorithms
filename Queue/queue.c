/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

void queue_init(struct Queue *queue) 		//initializes the queue before use
{
    queue->first = queue->last = NULL;	//set the first and the last pointers as null
    queue->size = 0;					//there's no items on queue yet, so 0
}

int queue_add(struct Queue *queue, int data)		//adds new item to queue, requiring the pointer to the queue and the value
{
    struct QueueItem *aux;                          //creating an auxiliary pointer to store a QueueItem...
    aux = malloc(sizeof (struct QueueItem));        //... then allocating space for it

    if(aux != NULL)                                 //if allocating space is successful...
    {
        aux->value = data;                          //... we need to save the integer value to the value field of the struct ...
        aux->next = NULL;                           //... and set the next QueueItem as null.
        if(queue->first == NULL)                    //if the first item of the queue is null...
        {
            queue->first = aux;                     //... then set aux as the first one.
        }
        else
        {
            queue->last->next = aux;                //if not, aux goes at the end of the existing queue, updating the last item's pointer
        }
        queue->last = aux;                          //we need to update the "last" pointer as the aux value
        queue->size++;                              //then updating the queue size
        return 0;                                   //success!
    }
    return 1;                                       //fail
}

int queue_checkfirst(struct Queue queue, int *data)     //return the first (front) value, receiving two pointers
{
    if(queue.first != NULL)                             //if the last value is not null ...
    {
        *data = queue.first->value;                     //... copy the first value to the value pointed by data ...
        return 0;                                       //... and return true.
    }
    return 1;                                           //or fail, of course
}

int queue_remove(struct Queue *queue, int *data)				//remove item passing queue pointer and data pointer (to put the removed value somewhere else
{
    if(queue->first != NULL)									//if the first item on queue is not null ...
    {
        struct QueueItem *aux;                                  //... create an auxiliary QueueItem pointer ...
        aux = queue->first;                                     //... to store the first item on queue ...
        *data = aux->value;                                     //... and save the value somewhere else ...
        queue->first = aux->next;                               //... to set the first item as the next one.
        if(queue->first == NULL) queue->last = NULL;            //if the first is already null, the last must be null too
        free(aux);                                              //let's free the auxiliary (that is the former first item on the queue)
        queue->size--;                                          //and decrease the queue size
        return 0;                                               //sucess!
    }
    return 1;													//fail
}

int queue_size(struct Queue queue)	//return the queue size
{
    int size = queue.size;
    return size;
}
