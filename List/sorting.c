/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int list_is_sorted(struct List *list)       //check if list is sorted - requires a pointer to the list
{
    struct ListItem *aux = NULL;            //an iterator, again named aux
    if(list->size < 2)                      //if there are 0 or 1 item, the list is obviously sorted
        return 1;

    aux = list->first;                      //but if there are more items, our iterator need to get the first item
    while (aux->next != NULL)               //running the loop until the penultimate item
    {
        if(aux->value > aux->next->value)   //if there are any value who is followed by a lower value, it's not sorted
            return 1;

        aux = aux->next;                    //update aux to the next
    }
    return 0;                               //if can reach here, it's sorted
}

//SORTING ALGORITHMS STARTS HERE
void list_bubblesort(struct List *list)                 //bubblesort - a basic sorting algorithm, swaps neighbours if one is higher than it's next - requires a pointer to the list
{
    struct ListItem *aux, *before;                      //bubblesort requires at least 2 pointers to iterate around the list

    if(list == NULL || list->first == NULL) return;     //if there are no list or if the list is empty, there's nothing to do

    while(list_is_sorted(list) != 0)                    //while the list is not sorted, keep running the loop
    {
        aux = list->first->next;                        //set aux as the list's first's next, it'll be used check the item ahead to see if it's higher to before's
        before = list->first;                           //set before as the list's first
        while(aux != NULL)                              //while loop to run through the list
        {
            if(before->value > aux->value)              //if before's value is higher than aux's value (aux is ahead of before), swap the values
            {
                int xchange = before->value;
                before->value = aux->value;
                aux->value = xchange;
            }
            before = aux;                               //set before as aux in order to continue until the end
            aux = aux->next;                            //and set aux as aux's next
        }
    }
}

void list_insertionsort(struct List *list)                      //insertionsort - a sorting algorithm that arranges a sorted output list one item at a time
{
    struct List sorted;                                         //the output list
    list_init(&sorted);                                         //initializing the output list
    struct ListItem *aux = list->first;                         //an iterator starting at the beginning of the list

    if(list == NULL || list->first == NULL) return;             //if nothing (no list or no content on list), nothing to do

    while(aux != NULL)                                          //while loop to run through the list
    {
        struct ListItem *next = aux->next;                      //another iterator, regarding the next item on list
        if(list_add_sorted(&sorted, aux->value) != 0) return;   //try to add the value in the sorted output list using our sorted addition
        else list_remove(list, aux->value);                     //the reason of this line it's because we're going to replace our input list by the output sorted list, otherwise this is not necessary
        aux = next;                                             //moves on to the next item
    }

    *list = sorted;                                             //we did it because I wanted to replace the lists, otherwise it's not needed
}
//SORTING ALGORITHMS ENDS HERE

