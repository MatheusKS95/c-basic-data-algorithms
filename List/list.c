/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "list.h"

void list_init(struct List *list)     //initializes the list to make it usable, requiring a List pointer
{
    list->first = list->last = NULL;  //set the queue's first and last pointers as null, because there are no items listed yet
    list->size = 0;                   //if there are no items listed, the list's size is 0
}

int list_add_end(struct List *list, int add)                    //adds a new item to list, adding as the last item, disregarding any kind of sorting - requires a pointer to the list and the value to be added
{
    struct ListItem *aux;                                       //creates an auxiliary ListItem in order to save the add value later
    aux = NULL;                                                 //set aux as NULL just to be sure

    aux = (struct ListItem *)malloc(sizeof(struct ListItem));   //allocates a chunk of memory to aux
    if(aux != NULL)                                             //if the allocating was successful...
    {
        aux->value = add;                                       //... we have to save the value in aux...
        aux->next = NULL;                                       //... and set the next item as NULL, because we are adding to the end of the list.
        if(list->first == NULL)                                 //if the first item on list is null...
        {
            aux->prev = NULL;                                   //...we set the aux's pointer to the previous as null...
            list->first = aux;                                  //...and the list's first as aux.
        }
        else                                                    //if the first item is not null...
        {
            aux->prev = list->last;                             //...the aux's previous is the last item on list...
            list->last->next = aux;                             //...and the last's next item is aux now.
        }
        list->last = aux;                                       //the last item is aux
        list->size++;                                           //finally updating the list's size.
        return 0;
    }
    return 1;
}

int list_add_sorted(struct List *list, int add)                 //adds a new item on the list, but inserts it in the right place between a lower and a higher value - requires a pointer to the list and the value to be added
{
    struct ListItem *newitem, *aux, *behind;                    //creates auxiliars ListItems in order to save and manage the list in order to save the value properly without blowing the list up

    newitem = malloc(sizeof(struct ListItem));                  //some allocations
    aux = malloc(sizeof(struct ListItem));
    behind = malloc(sizeof(struct ListItem));

    if(newitem != NULL && aux != NULL && behind != NULL)        //if the allocations were successful...
    {
        newitem->value = add;                                   //we need to set the value the item to be added as 'add'

        if(list->first == NULL)                                 //if there's NO ITEMS on the list...
        {
            list->first = newitem;                              //...we set the first item on the list as the recently created newitem, with the value...
            list->last = newitem;                               //...and the last item is also newitem, because there are no items on the list.
            list->last->next = NULL;                            //since it's only one item, the last item MUST NOT have a "next one"
            list->first->prev = NULL;                           //same goes to the first item, who can't have a "previous one"
            list->size++;                                       //then finally we can update the list size
            return 0;
        }

        //I added this in order to prevent repeating values - needs to be fixed later
        behind = NULL;
        aux = list->first;
        while(aux != NULL)
        {
            if(add == aux->value)
            {
                free(newitem);
                return 1;
            }
            behind = aux;
            aux = aux->next;
        }

        if(add < list->first->value)                            //if the newest value is the LOWEST...
        {
            newitem->next = list->first;                        //...the newest' next value is the first value on the list...
            newitem->prev = NULL;                               //...and doesn't have a previous one
            list->first = newitem;                              //finally, update the first as the newitem...
            list->size++;                                       //and update the total list size
            return 0;
        }
        if(add > list->last->value)                             //if the newest value is the HIGHEST...
        {
            newitem->next = NULL;                               //...it doesn't have a next one...
            newitem->prev = list->last;                         //...and the previous is the list's current last item
            list->last->next = newitem;                         //the last item have a next now, newitem
            list->last = newitem;                               //finally, updating the list's last item, it's now newitem
            list->size++;                                       //updating the list size
            return 0;
        }
        if(add > list->first->value && add < list->last->value) //if the newest value is NEITHER lowest or highest
        {
            //2 iterators are required in this doubly linked list
            aux = list->first->next;                            //aux is the list's second item
            behind = list->first;                               //behind is the first

            while(aux != NULL)                                  //keep running through all the list using aux as the main iterator
            {
                if(add < aux->value)                            //if the value to be added is lower than the value pointed by aux...
                {
                    newitem->next = aux;                        //...we need to set the newest item to have aux as it's next item...
                    newitem->prev = behind;                     //...and behind is the newitem's previous item...
                    behind->next = newitem;                     //...then we update the previous item's next one as the newitem (yeah, sounds confusing)...
                    list->size++;                               //then we can update the list size
                    return 0;
                }
                behind = aux;                                   //while not entering previous if, set behind as aux, because...
                aux = aux->next;                                //...aux is now aux's next, in order to run through the list until the last one
            }
            return 1;
        }
    }
    return 1;
}

int list_check_if_exists(struct List list, int val) //search through the list for a given int - requires the list (though not a pointer) and a integer value
{
    struct ListItem *aux;                           //creates an auxiliary "iterator"...
    aux = list.first;                               //...who receives the pointer to the first item
    while(aux != NULL)                              //then put aux in a loop until the end of the list
    {
        if(aux->value == val) return 0;             //if the value in aux is the same as given, returns 1 (exists)
        aux = aux->next;                            //but if not, set aux as aux's next until aux is null
    }
    return 1;
}

int list_remove(struct List *list, int remove)          //searches and removes a given element from the list - requires a pointer to the list and a value to be searched and removed
{
    struct ListItem *aux, *behind;                      //again, a doubly linked link requires 2 "iterators" for most of operations
    aux = behind = NULL;                                //both set as null just to be sure
    if(list->first != NULL)                             //if the list is not empty...
    {
        aux = list->first;                              //...set aux as first...
        //behind = NULL;                                //...we don't set behind yet
        while(aux != NULL)                              //first we need to search if the element exists...
        {
            if(aux->value == remove) break;             //if yes, we leave the search
            behind = aux;                               //if not, we set behind as the actual value of aux...
            aux = aux->next;                            //...and aux as aux's next, in order to go to the next item until list ends
        }

        if(aux == NULL)                                 //if aux ended null, the value was not found.
        {
            return 1;
        }

        if(list->first == list->last)                   //if the value found is the ONLY ONE in the list...
        {
            list->first = list->last = NULL;            //...we set the list's last and first as null, because we will remove the value later
        }
        else                                            //if there are MORE items on the list...
        {
            if(aux == list->first)                      //...and it's the FIRST item on list...
            {
                list->first = list->first->next;        //...we just set the first as the next of the actual first...
            }
            else if (aux == list->last)                 //...but the value to be removed is the LAST...
            {
                list->last = behind;                    //...behind becomes the last item of the list...
                list->last->next = NULL;                //...and we need to remove the now last's next pointer.
            }
            else                                        //however, if it's NEITHER the first NOR the last...
            {
                behind->next = aux->next;               //...we set the behind's next as the aux's next
            }
        }

        free(aux);                                      //all the previous operations of updating next's and previous's pointers is because we will wipe aux from memory
        list->size--;                                   //finally we can update the list's size
        return 0;
    }
    return 1;
}

void list_show_all(struct List list)            //show all items using a printf - requires the list
{
    struct ListItem *aux;                       //a "iterator" in order to get the value of each item
    aux = NULL;
    if(list.first == NULL)                      //if there are no items on the list, it's simply empty
    {
        printf("\nEMPTY LIST\n\n");
    }
    else                                        //but if not empty...
    {
        aux = list.first;                       //...we set the first item as the aux starting line
        printf("\n");
        while(aux != NULL)                      //if there are items on the list, repeat until the end...
        {
            printf("%d <-> ", aux->value);      //...the show of values...
            aux = aux->next;                    //...and update aux to aux's next, in order to keep the loop running until finish
        }
        printf("\n\n");
    }

}

int list_sum(struct List list)  //sum all items' values
{
    int sum = 0;

    struct ListItem *aux;
    aux = list.first;

    while(aux != NULL)
    {
        sum += aux->value;      //some basic sum based on current sum value + item on list
        aux = aux->next;        //then move on to the next item
    }

    return sum;
}

int list_max(struct List list)
{
    int max;

    struct ListItem *aux;
    aux = list.first;
    max = aux->value;

    while(aux != NULL)
    {
        if(aux->value > max)
        {
            max = aux->value;
        }
        aux = aux->next;
    }

    return max;
}

int list_min(struct List list)
{
    int min;

    struct ListItem *aux;
    aux = list.first;
    min = aux->value;

    while(aux != NULL)
    {
        if(aux->value < min)
        {
            min = aux->value;
        }
        aux = aux->next;
    }

    return min;
}

void list_export(struct List list)              //exact same functionality of list_show_all but saves on a file instead
{
    FILE *fp = fopen("export.txt", "w");        //new file
    struct ListItem *aux;                       //a "iterator" in order to get the value of each item
    aux = NULL;
    if(list.first == NULL)                      //if there are no items on the list, it's simply empty
    {
        fprintf(fp, "EMPTY LIST");
    }
    else                                        //but if not empty...
    {
        aux = list.first;
        while(aux != NULL)                      //if there are items on the list, repeat until the end...
        {
            fprintf(fp, "%d ", aux->value);     //...the show of values...
            aux = aux->next;                    //...and update aux to aux's next, in order to keep the loop running until finish
        }
    }
    fclose(fp);
}

int list_import(struct List *list, char *filename)
{
    FILE *fp = fopen(filename, "r");            //opens the file
    int status = 0;                             //error control flag
    int aux;                                    //holds the value from file temporarily to include on list

    if(fp == NULL)
    {
        return 1;                               //if no file is found, just end
    }

    while(fscanf(fp, "%d ", &aux) > 0)          //while there are values on the string from the file
    {
        if(list_add_end(list, aux) == 1)        //try to add, if fails, set status as 1
        {
            status = 1;
        }
    }

    fclose(fp);                                 //closes the file
    return status;
}
