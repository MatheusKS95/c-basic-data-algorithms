/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LIST_C
#define LIST_C

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

struct ListItem             //ListItem is an individual item of the list, with 2 pointers and the value
{
    struct ListItem *prev;  //pointer to the previous item in the list
    int value;              //the value itself, change if you need a char, float, double or something else
    struct ListItem *next;  //pointer to the next item in the list
};

struct List
{
    struct ListItem *first; //pointer to the first item in the list
    struct ListItem *last;  //pointer to the last item in the list
    int size;               //the total size of the list
};


void list_init(struct List *list);                      //List initializer

int list_add_end(struct List *list, int add);           //Add to the end of the list
int list_add_sorted(struct List *list, int add);        //Add to a sorted position of the list
int list_check_if_exists(struct List list, int val);    //Check if a certain value is currently in the list
int list_remove(struct List *list, int remove);         //Remove an item from the list and free the memory
void list_show_all(struct List list);                   //Show all the items in a string
int list_sum(struct List list);                         //Sum all the values in the list (won't work on non-numeric lists)
int list_max(struct List list);                         //Returns the list's largest value
int list_min(struct List list);                         //Returns the list's smallest value
void list_export(struct List list);                     //Export the list to a text file
int list_import(struct List *list, char *filename);     //Import a list from a text file

int list_is_sorted(struct List *list);                  //Checks if the list is sorted
void list_bubblesort(struct List *list);                //Applies a bubblesort algorithm to the list, sorting it's values
void list_insertionsort(struct List *list);             //Applies an insertionsort algorithm to the list, sorting it's values

struct ListItem *list_search_sequential(struct List list, int value, int *position); //Search sequentially through the list looking for a value

#endif
