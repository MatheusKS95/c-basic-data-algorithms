/*
*Copyright 2020 Matheus Klein Schaefer
*
*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
*files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
*modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
*is furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
*OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
*LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
*IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//the following code is intended to make use of list functions

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main()
{
    struct List listy;
    //listy = (struct List *)malloc(sizeof(struct List));

    list_init(&listy);

    printf("DOUBLY LINKED LIST\n");

    char opc;
    char *filename; //for imports
    size_t size_filename = 30;

    do{
        int valor;
        int stats;
        printf("Please select an option:\n");
        printf("a) Add value to end of the list\n");
        printf("b) Add value to sorted position - not tested\n");
        printf("c) Check if value exists\n");
        printf("r) Remove value\n");
        printf("s) Show all values\n");
        printf("p) Sum all values\n");
        printf(">) Max value\n");
        printf("<) Min value\n");
        printf("e) Export list to file\n");
        printf("i) Import list from file\n");
        printf("d) Check if list is sorted\n");
        printf("h) Sort (via insertionsort - current code supports also bubblesort)\n");
        printf("q) Search (sequentially)\n");
        printf("x) Exit - DO NOT EXIT IF LIST IS NOT EMPTY - MAY CAUSE MEMORY LEAK\n");
        scanf(" %c", &opc);

        switch(opc)
        {
            case 'a': printf("\nInsert a value: ");
                      scanf("%d", &valor);
                      stats = list_add_end(&listy, valor);
                      if(stats == 0)
                          printf("\nSuccessfully added as the last in the list.\n");
                      else
                          printf("\nError\n");
                      break;
            case 'b': printf("\nInsert a value: ");
                      scanf("%d", &valor);
                      stats = list_add_sorted(&listy, valor);
                      if(stats == 0)
                          printf("\nSuccessfully added in the right place in the list.\n");
                      else
                          printf("\nError\n");
                      break;
            case 'c': printf("\nInsert a value: ");
                      scanf("%d", &valor);
                      stats = list_check_if_exists(listy, valor);
                      if(stats == 0)
                          printf("\nYes, it exists\n");
                      else
                          printf("\nNo, it doesn't exist\n");
                      break;
            case 'r': printf("\nInsert a value to be removed, if it exists: ");
                      scanf("%d", &valor);
                      stats = list_remove(&listy, valor);
                      if(stats == 0)
                          printf("\nSuccessfully removed from the list.\n");
                      else
                          printf("\nError\n");
                      break;
            case 's': list_show_all(listy);
                      break;
            case 'p': printf("\nList sum: %d\n", list_sum(listy));
                      break;
            case '>': if(listy.size > 0)
                      {
                          printf("\nLargest value: %d\n", list_max(listy));
                      }
                      else
                      {
                          printf("\nNo items on list\n");
                      }
                      break;
            case '<': if(listy.size > 0)
                      {
                          printf("\nSmallest value: %d\n", list_min(listy));
                      }
                      else
                      {
                          printf("\nNo items on list\n");
                      }
                      break;
            case 'e': list_export(listy);
                      break;
            case 'i': filename = (char *) malloc(size_filename);
                      printf("\nType the file name (max 30 characters): ");
                      getchar();
                      getline(&filename, &size_filename, stdin);
                      int len = strlen(filename);
                      if(filename[len - 1] == '\n')
                      {
                          filename[len - 1] = 0;
                      }
                      stats = list_import(&listy, filename);
                      if(stats == 0)
                      {
                          printf("\nSuccessfully imported to the list!\n");
                      }
                      else
                      {
                          printf("Error when importing from file...\n");
                      }
                      free(filename);
                      break;
            case 'd': stats = list_is_sorted(&listy);
                      if(stats == 0)
                          printf("\nSorted!\n");
                      else
                          printf("\nAssorted...\n");
                      break;
            //currently 2 sorting algorithms available in this project: list_bubblesort and list_insertionsort
            case 'h': list_insertionsort(&listy);
                      list_show_all(listy);
                      break;
            case 'q': printf("\nValue to be searched: ");
                      scanf("%d", &valor);
                      struct ListItem *result = list_search_sequential(listy, valor, &stats);
					  if(result != NULL)
						  printf("\nItem is located at %d. It's value is %d\n", stats, result->value);
					  else
						  printf("\nNot found.\n");
                      break;
            default: break;
        }
    }while(opc != 'x');
    return 0;
}
